
public class Grading 
{

	public static String courseMark(int exam, int courseWork)
	{
		int total = exam+courseWork;
		
		if(total <= 100 && total >=70)
		{
			return "A";
		}
		else if(total < 70 && total >= 50)
		{
			return "B";
		}
		else if(total < 50 && total >= 30)
		{
			return "C";
		}
		else if(total < 30 && total >=0)
		{
			return "D";
		}
		else
		{
			return "FM";
		}
	}

	public static void main(String[] args) 
	{
		System.out.println("Your grade is " +courseMark(90, 90));
	}
}
