import junit.framework.TestCase;

public class GradingTest extends TestCase 
{ 
	//EP
	
	//Test #: 1
	//Objective: Course mark range of 100-70
	//Input(s): 80, 81
	//Expected Output(s): 'A'
	
	public void testcourseMark001()
	{
		assertEquals("A", Grading.courseMark(80, 81));
	}
	
	//Test #: 2
	//Objective: Course mark range of 69-50
	//Input(s):	55, 57
	//Expected Output(s): 'B'
	
	public void testcourseMark002()
	{
		assertEquals("B", Grading.courseMark(55, 57));
	}
	
	//Test #: 3
	//Objective: Course mark range of 49-30
	//Input(s): 31,39
	//Expected Output(s): 'C'
	
	public void testcourseMark003()
	{
		assertEquals("C", Grading.courseMark(31, 39));
	}
	
	//Test #: 4
	//Objective: Course mark range of 29-0
	//Input(s): 25, 10
	//Expected Output(s): 'D'
	
	public void testcourseMark004()
	{
		assertEquals("D", Grading.courseMark(25, 10));
	}
	
	//Test #: 5
	//Objective: Course mark range of 101-MAX INT
	//Input(s): 500, 650
	//Expected Output(s): "FM"
	
	public void testcourseMark005()
	{
		assertEquals("FM", Grading.courseMark(500, 650));
	}
	
	//Test #: 6
	//Objective:  Course mark range of -1-MIN INT
	//Input(s): -125, -158
	//Expected Output(s): "FM"
	
	public void testcourseMark006()
	{
		assertEquals("FM", Grading.courseMark(-125, -158));
	}
	
	//BVA
	
	//Test #: 7
	//Objective: Course mark value of 100 
	//Input(s): 100, 100
	//Expected Output(s): 'A'
	
	public void testcourseMark007()
	{
		assertEquals("A", Grading.courseMark(100, 100));
	}
	
	//Test #: 8
	//Objective: Course mark value of 70
	//Input(s): 70, 70
	//Expected Output(s): 'A'
	
	public void testcourseMark008()
	{
		assertEquals("A", Grading.courseMark(70, 70));
	}
	
	//Test #: 9
	//Objective: Course mark value of 69
	//Input(s): 69, 69
	//Expected Output(s): 'B' 
	
	public void testcourseMark009()
	{
		assertEquals("B", Grading.courseMark(69, 69));
	}
	
	//Test #: 10
	//Objective: Course mark value of 50
	//Input(s): 50, 50
	//Expected Output(s): 'B'
	
	public void testcourseMark010()
	{
		assertEquals("B", Grading.courseMark(50, 50));
	}
	
	//Test #: 11
	//Objective: Course mark value of 49
	//Input(s): 49, 49
	//Expected Output(s): 'C'
	
	public void testcourseMark011()
	{
		assertEquals("C", Grading.courseMark(49, 49));
	}
	
	//Test #: 12
	//Objective: Course mark value of 30
	//Input(s): 30, 30
	//Expected Output(s): 'C'
	
	public void testcourseMark012()
	{
		assertEquals("C", Grading.courseMark(30, 30));
	}
	
	//Test #: 13
	//Objective: Course mark value of 29
	//Input(s): 29, 29
	//Expected Output(s): 'D'
	
	public void testcourseMark013()
	{
		assertEquals("D", Grading.courseMark(29, 29));
	}
	
	//Test #: 14
	//Objective: Course mark value of 0
	//Input(s): 0
	//Expected Output(s): 'D'
	
	public void testcourseMark014()
	{
		assertEquals("D", Grading.courseMark(0, 0));
	}
	
	//Test #: 15
	//Objective: Course mark of 101
	//Input(s): 101, 101
	//Expected Output(s): "FM"
	
	public void testcourseMark015()
	{
		assertEquals("FM", Grading.courseMark(101, 101));
	}
	
	//Test #: 16
	//Objective: Course mark of -1
	//Input(s): -1
	//Expected Output(s): "FM"
	
	public void testcourseMark016()
	{
		assertEquals("FM", Grading.courseMark(-1, -1));
	}
	
	//Test #: 17
	//Objective: Invalid value of MIN INT
	//Input(s): Integer.MIN_VALUE, Integer_MIN.VALUE
	//Expected Output(s): "FM"
	
	public void testcourseMark017()
	{
		assertEquals("FM", Grading.courseMark(Integer.MIN_VALUE, Integer.MIN_VALUE));
	}
	
	//Test #: 18
	//Objective: Invalid value of MAX INT
	//Input(s): Integer.MAX_VALUE, Integer_MAX.VALUE
	//Expected Output(s): "FM"
	
	public void testcourseMark018()
	{
		assertEquals("FM", Grading.courseMark(Integer.MAX_VALUE, Integer.MAX_VALUE));
	}
}
